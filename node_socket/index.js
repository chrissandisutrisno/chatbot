const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const path = require('path');
const { Server } = require("socket.io");
const axios = require('axios').default;
const io = new Server(server);
const PORT = 3001

app.get("/", (req, res) => {
    res.sendFile(path.join(`${__dirname}/index.html`));
});

io.on('connection', (socket) => {
    io.to(socket.id).emit('get_msg',"Hello :)");
    socket.on("send_msg" , (question) => {
        clearTimeout(socket.inactivityTimeout);
        socket.inactivityTimeout = setTimeout(() => {
            socket.disconnect(true)
        }, 10000);
        axios.post("http://127.0.0.1:8000/api/chat-bot/" , {
            question:question
        }).then((response)=>{
            let answer = response.data.data.answer
            io.to(socket.id).emit('get_msg', answer);
        });
    })
});
server.listen(PORT, () => {
    console.log(`listening on *:${PORT}`);
});