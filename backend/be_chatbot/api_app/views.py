from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .bot import Bot
from .serializers import *
from .models import *


bot = Bot('question_intent.csv')
bot.train()

class ChatBotViews(APIView):
    def post(self,request):
        question = request.data['question']
        result = bot.reply(question)
        answer = {"question" : question , 'answer' : result }
        print(request.data)
        serializer = ChatBotSerializer(data=answer)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "success", "data": serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response({"status": "error", "data": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

