from rest_framework import serializers
from .models import *

class ChatBotSerializer(serializers.ModelSerializer):
    question = serializers.CharField(max_length=1000)
    answer = serializers.CharField(max_length=1000)
    class Meta:
        model = ChatBot
        fields = ('__all__')
