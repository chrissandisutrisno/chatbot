from django.urls import path
from .views import *

urlpatterns = [
    path('chat-bot/',ChatBotViews.as_view())
]