import pandas as pd
import numpy as np
from sklearn.svm import SVC
import spacy

class Bot:
    def __init__(self,csvpath):
        self.nlp =spacy.load('en_core_web_md')
        self.loadData(csvpath)
        self.model = SVC(C=1, gamma="auto", probability=True)
        self.responses_ml = {
            "dinner_preference":"Pasta with salmon and red pesto please!",
            "arrival_time": "I will be home around 6 pm.",
            "default":"I like you too!"
        }

    def loadData(self,url):
        question_data = pd.read_csv(url)
        self.question = question_data.question
        self.intent = question_data.intent

    def train(self):
        X_train = np.zeros((len(self.question),self.nlp('sentences').vocab.vectors_length))
        for i, sentence in enumerate(self.question):
            # Pass each each sentence to the nlp object to create a document
            doc = self.nlp(sentence)
            # Save the document's .vector attribute to the corresponding row in X
            X_train[i, :] = doc.vector
        self.model.fit(X_train,self.intent)
        print(self.model)

    def reply(self, input):
        doc = self.nlp(input)
        res_intent = self.model.predict([doc.vector])[0]
        max_proba = max(self.model.predict_proba([doc.vector])[0])
        if(max_proba <= 0.5):
            self.response = "No answer"
        else:
            self.response = self.responses_ml.get(res_intent, self.responses_ml["default"])
        # self.response = self.responses_ml.get(res_intent, self.responses_ml["default"])
        return self.response





# bot = Bot("question_intent.csv")
# bot.train()
# a= bot.reply("What would you like to have for dinner")
# print(a)