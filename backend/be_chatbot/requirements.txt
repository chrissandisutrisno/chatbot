# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: win-64
django=3.2.5=pyhd3eb1b0_0
djangorestframework=3.12.4=pyhd8ed1ab_0
pandas=1.3.3=py38h6214cd6_0
numpy=1.20.3=py38ha4e8547_0
numpy-base=1.20.3=py38hc2deb75_0
numpydoc=1.1.0=pyhd3eb1b0_1
spacy=3.1.3=py38he5193b3_0
spacy-legacy=3.0.8=pyhd8ed1ab_0
en-core-web-md=3.1.0=pypi_0
path=16.0.0=py38haa95532_0
path.py=12.5.0=hd3eb1b0_0
pathlib2=2.3.6=py38haa95532_2
pathspec=0.7.0=py_0
pathtools=0.1.2=pyhd3eb1b0_1
pathy=0.6.0=pyhd8ed1ab_0
