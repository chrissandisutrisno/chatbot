# ChatBot NLP

Simple chat bot for prosa.ai test

## File Structure
In this repo we use microservices  architecture there is two folder
Backend
node_server

## Installation
In this repo we already create docker file to make build process more easier
For django server you can follow this
```bash
cd backend/be_chatbot
docker build -tag backend .
```


To build node_socket client you can follow this

```bash
cd node_socket
docker build -tag front-end  .
```

